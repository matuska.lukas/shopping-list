<?php  //include realpath($_SERVER['DOCUMENT_ROOT']).'/htmlParts/header.php'; ?>
<html>
    <head>
        <title>403 Přístup odepřen!</title>
        <meta property="og:type" content="Error page" />
        <meta property="og:image" content="/background.jpg" />
        <meta property="og:description" content="Error page" />
    </head>
    <body style="background-color: transparent; ">
        <div style="width:100%; " class="mui-container">
            <div class="mui-panel" style="text-align:center">
                <h1 style="text-align:center">403 Přístup odepřen!</h1>
                <img src="/images/403.png" height="100%" style="height: 100%;">
                <?php  //include realpath($_SERVER['DOCUMENT_ROOT']).'/htmlParts/menu.php'; ?>
                <?php  //include realpath($_SERVER['DOCUMENT_ROOT']).'/htmlParts/footer.php'; ?>
            </div>
        </div>
    </body>
</html>
