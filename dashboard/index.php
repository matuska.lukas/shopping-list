<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>Nákupní seznam</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php
    /* ----- SESSION  ----- */
    require_once '../sessions/session.php'; 
    
    /* ----- DATABASE ----- */
    require_once '../config/config.php'; // load credentials from config file
    require_once '../database/connect.php'; // connect to database

    /* ----- DELETE ----- */
    $id_user = $mysqli->query("SELECT id FROM shopping_user WHERE `username`='$username';")->fetch_array()["id"];
    if (isset($_POST['delete'])) {
        $delete = $mysqli->real_escape_string($_POST['delete']);
        $mysqli->query("DELETE FROM sape_shopping WHERE `id`= $delete;");
    }

    /* ----- INSERT ----- */
    if (isset($_POST["addValue"])) {
        $description = $mysqli->real_escape_string($_POST["description"]);
        $count = $mysqli->real_escape_string($_POST["count"]);
        
        if ($_POST["price"] != "") {
            $price = $mysqli->real_escape_string($_POST["price"]);
        }else{
            $price = "0";
        }

        $insertSql = $mysqli->prepare("INSERT INTO `sape_shopping` (`description`, `count`, `price`, `id_user`) VALUES (?, ?, ?, ?);");
        $insertSql->bind_param('sidi', $description, $count, $price, $id_user);
        if (!$insertSql->execute()) {
            die("<p style='color: red; '>".$insertSql->error."</p>");
        }else{
            ?><p style='color: green;'>Úspešně přidáno!</p><?php
        }
    }

    /* ----- NAVBAR ----- */
    require_once '../htmlParts/navbar.php';
    ?>
    <header id="main-header">
        <div class="darken">
            <div class="container">
                <h1>Nákupní seznam</h1>
                <p>Co nesmím zapomenout koupit?</p>
                <form method="POST" action="#">
                    <div class="form-row">
                        <input type="text" name="description" placeholder="Zboží" required>
                        <input type="number" name="count" placeholder="Počet" required>
                        <input type="number" name="price" placeholder="Cena za kus" step="0.01">
                    </div>
                    <input class="submitButton" type="submit" name="addValue" value="Přidat k seznamu">
                </form>
            </div>
        </div>
    </header>
    <div id="main">
        <div class="container">
            <div class='dropdown'>
                <p class='dropbtn'>Řadit&nbsp;podle:</p>
                <div class='dropdown-content'>
                    <a href='?sortBy=priceDescending'>Od největší ceny</a>
                    <a href='?sortBy=priceAscending'>Od nejmenší ceny</a>
                    <a href='?sortBy=dateDescending'>Podle&nbsp;data&nbsp;přidání&nbsp;&#8593;</a>
                    <a href='?sortBy=dateAscending'>Podle&nbsp;data&nbsp;přidání&nbsp;&#8595;</a>
                </div>
            </div>
            <div class='dropdown'>
                <p class='dropbtn'>Filtrovat&nbsp;podle:</p>
                <div class='dropdown-content'>
                    <a href='?filterBy=all'>Vše</a>
                    <a href='?filterBy=price'>S&nbsp;cenou</a>
                    <a href='?filterBy=withoutPrice'>Bez&nbsp;ceny</a>
                </div>
            </div>
            <ul id="shopping-list">
                <?php
                    /* ----- SELECT ----- */

                    /* ----- Sorting ---- */
                    if (isset($_GET['sortBy'])){
                        switch ($_GET['sortBy']){
                            case 'dateAscending':
                                $selectSqlOrder = "ORDER BY `id`";
                                break;
                            case 'dateDescending':
                                $selectSqlOrder = "ORDER BY `id` DESC";
                                break;
                            case 'priceDescending':
                                $selectSqlOrder = "ORDER BY (`price` * `count`) DESC";
                                break;
                            case 'priceAscending':
                                $selectSqlOrder = "ORDER BY  (`price` * `count`) ASC";
                                break;
                            default:
                                $selectSqlOrder = '';
                                break;
                        }
                    }else {
                        $selectSqlOrder = '';
                    }
                    
                    /* ----- Filtering ---- */
                    if (isset($_GET['filterBy'])){
                        switch ($_GET['filterBy']){
                            case 'all':
                                $selectSql = "SELECT * FROM sape_shopping WHERE `id_user` = '$id_user' $selectSqlOrder;";
                                break;
                            case 'price':
                                $selectSql = "SELECT * FROM sape_shopping WHERE `id_user` = '$id_user' AND `price` != '' $selectSqlOrder;";
                                break;
                            case 'withoutPrice':
                                $selectSql = "SELECT * FROM sape_shopping WHERE `id_user` = '$id_user' AND `price` = '' $selectSqlOrder;";
                                break;
                        }
                    }
                    
                    /* ----- DEFAULT option for SQL ---- */
                    if (!isset($selectSql)){
                        $selectSql = "SELECT * FROM sape_shopping WHERE `id_user` = '$id_user' $selectSqlOrder;";
                    }
                    $selectQuery = $mysqli->query($selectSql);
                    if (!$selectQuery) {
                        die("<p style='color: red; '>".$mysqli->error."</p>");
                    }

                    while ($row = $selectQuery->fetch_array()) {
                        ?>
                        <li>
                            <div class="description"><?php echo $row["description"];?></div>
                            <div class="count"><?php echo $row["count"]; ?></div>
                            <div class="price">
                                <?php
                                if ($row["price"] != 0){
                                    echo number_format($row["price"] * $row["count"], 2) . " CZK";
                                }else{
                                    echo "-";
                                }
                                ?>
                                <!--<img src='/images/x-button.png' height='30px'>-->
                                <!--<input type="image" src="/images/x-button.png" alt="Submit Form" height='30px'/>-->
                                <!--<p>&#10005;</p>-->
                            </div>
                            <div class='delete'>
                                <form action='' method='POST'>
                                    <input type='text' name='delete' value='<?php echo $row['id'];?>' style='display: none;'>
                                    <input class='deleteButton' type='submit' value='&#10005;'>
                                </form>
                            </div>
                        </li>
                        <?php
                    }
                ?>
                    </table>
                </form>
            </ul>
        </div>
    </div>
</body>
</html>
