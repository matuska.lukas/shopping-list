<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>Profil</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
    <?php
    /* ----- SESSION ----- */
    require_once './sessions/session.php'; 
    
    /* ----- DATABASE CONNECTION ----- */
    require_once './config/config.php'; // load credentials from config file
    require_once './database/connect.php'; // connect to database

    $id_user = $mysqli->query("SELECT id FROM shopping_user WHERE `username`='$username';")->fetch_array()["id"];

    /* ----- NAVBAR ----- */
    require_once './htmlParts/navbar.php';
    ?>

    <header id="main-header">
        <div class="darken">
            <div class="container">
                <h1>Můj profil</h1>
                <p>Teď už konečně vím, co o mě tato stránka ví.</p>
            </div>
        </div>
    </header>
    <div id="main">
        <div class="container">
            <?php
                $selectSql = "SELECT * FROM shopping_user WHERE `username`='$username'";
                $selectQuery = $mysqli->query($selectSql);
                $selectRow = $selectQuery->fetch_array();
            ?>
            <table id="shopping-list">
                <tr>
                    <td>Jméno:</td>
                    <td><?php echo $selectRow['forename'] . ' ' . $selectRow['surname']; ?></td>
                </tr>
                <tr>
                    <td>Uživatelské&nbsp;jméno:</td>
                    <td><?php echo $selectRow['username']; ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?php echo $selectRow['email']; ?></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
