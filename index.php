<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>Přihlášení - Nákupní seznam</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="/images/wallpaper.jpg" />
</head>
<body>
    <?php
    /* ----- DATABASE CONNECTION ----- */
    require_once './config/config.php';
    require_once './sessions/login.php';

    if(isset($_SESSION['login_user'])){
        header("location: dashboard/");
        die;
    }
    ?>

    <header id="main-header">
        <div class="darken">
            <div class="container">
                <h1>Přihlášení do nákupního seznamu</h1>
                <p>Hej mami! Jaké mám to heslo?</p>
                <form method="post" action="#">
                    <div class="form-row">
                        <input type="text" name="username" placeholder="Uživatelské jméno" required>
                        <input type="password" name="password" placeholder="Moje tajné heslo" required>
                    </div>
                    <input class='submitButton' type="submit" name="submit" value="Přihlásit">
                    <br/>
                    <a href='/register' style='color: white; text-align: center;'>Ježíší na kříži já ještě nemám účet a chci se zaregistrovat!</a>
                </form>
                <?php
                    if($error != ''){
                        echo "
                    <div class='msg msg-error z-depth-3 scale-transition'>
                        $error
                    </div>";
                    }
                ?>
            </div>
        </div>
    </header>
    <div id="main">
        <div class="container">
            <?php
            if ($error != ''){
                ?><img src="https://www.copytrans.net/app/uploads/sites/3/2018/05/incorrect-password-meme-780x439.jpg"><?php
            }else{
                ?><img src="https://www.mememaker.net/api/bucket?path=static/img/memes/full/2017/May/5/18/i-need-you-to-log-in.jpg">
                  <img src="https://media.makeameme.org/created/if-you-register-5b5a1a.jpg"><?php
            }
            ?>
        </div>
    </div>
</body>
</html>
