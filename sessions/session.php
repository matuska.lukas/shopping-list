<?php
    require_once realpath($_SERVER['DOCUMENT_ROOT']).'/config/config.php';
    //require_once '../config/config.php';

    $mysqli = mysqli_connect($db_host, $db_user, $db_pass, $db_db);
    $mysqli->set_charset("utf8");

    // Starting Session
    session_start();
   
    if (isset($_SESSION['login_user'])) {
        // Storing Session
        $user_check = $_SESSION['login_user'];
    }else {
        header('Location: /');  // Redirecting to login page
    }

    // SQL Query To Fetch Complete Information Of User
    $sessionQuery = $mysqli->query("SELECT * FROM `shopping_user` WHERE username='$user_check'");
    $row = $sessionQuery->fetch_assoc();
    //print_r($row);
    $username = $row['username'];
    $forename = $row['forename'];
    $surname = $row['surname'];
    $email = $row['email'];
    
    if(!isset($username)){
        $mysqli->close(); // Closing connection to database
        header('Location: /'); // Redirecting to login page
    }
?>