<?php
    require_once realpath($_SERVER['DOCUMENT_ROOT']).'/config/config.php';
    session_start(); // Starting Session
    $error = ''; // Variable To Store Error Message
    if (isset($_POST['submit'])) {
        if (empty($_POST['username'])) {
            $error = "Zadané uživatelské jméno bylo prázdné!";
        }elseif (empty($_POST['password'])) {
            $error = "Zadané heslo bylo prázdné!";
        }else{
            $mysqli = mysqli_connect($db_host, $db_user, $db_pass, $db_db);
            $mysqli->set_charset("utf8");

            // To protect MySQL injection for Security purpose
            $password = stripslashes($mysqli->real_escape_string($_POST['password']));
            $username = stripslashes($mysqli->real_escape_string($_POST['username']));
            
            // SQL query to fetch information of registerd users and finds user match.
            $query = $mysqli->query("SELECT * FROM `shopping_user` WHERE `password`='$password' AND `username`='$username'");
            $rows = mysqli_num_rows($query);
            if ($rows == 1) {
                $row = $query->fetch_array();
                $_SESSION['login_user'] = $row['username']; // Initializing session
                if (isset($_POST['cookies']) && $_POST['cookie']){
                    set_cookie('username', $row['username'], time() + 3600*24*7*30);
                    echo $_COOKIE['username'];
                }
                header("location: dashboard.php"); // Redirecting to other page
            }else{
                $error = "Zadané uživatelské údaje nejsou platné!";
            }
            $mysqli->close(); // Closing Connection
        }
    }
?>