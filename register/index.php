<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title>Registrace - Nákupní seznam</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<?php
    require_once realpath($_SERVER['DOCUMENT_ROOT']).'/sessions/login.php';
    //require_once '../sessions/login.php'; // Require login script

    if(isset($_SESSION['login_user'])){
        header("location: /dashboard");
    }
    $error = "";
   
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        require_once realpath($_SERVER['DOCUMENT_ROOT']).'/config/config.php';
        $mysqli = mysqli_connect($db_host, $db_user, $db_pass, $db_db);
        $mysqli->set_charset("utf8");

        // user_name, password and email sent from form 
        $forename = $mysqli->real_escape_string($_POST['forename']);
        $surname = $mysqli->real_escape_string($_POST['surname']);
        $username = $mysqli->real_escape_string($_POST['username']);
        $password = $mysqli->real_escape_string($_POST['password']);
        $email = $mysqli->real_escape_string($_POST['email']);  
        
        // Check is this account existing
        $sql = "SELECT id FROM shopping_user WHERE `username` = '$username';";
        $result = $mysqli->query($sql);
        $row = $result->fetch_array(); 
        //$row = $result->fetch_array(MYSQLI_ASSOC); 
        //$count = $result->num_rows(); // If result not matches $userName, table row must be 0 row
        $count = mysqli_num_rows($result);// If result not matches $userName, table row must be 0 row

        if($count == 0) {
            $inserSql = $mysqli->prepare("INSERT INTO shopping_user (forename, surname, username, password, email) VALUES (?, ?, ?, ?, ?)");
            $inserSql->bind_param('sssss', $forename, $surname, $username, $password, $email);
            if ($inserSql->execute()) {
                require_once realpath($_SERVER['DOCUMENT_ROOT']).'/sessions/session.php'; // Require login script
            } else {
                echo "Error with sql command.<br>" . $mysqli->error;
            }
        }else {
            $error = "Vámi zvolené uživatelské jméno již použil někdo před Vámi. :(";
        }
        $mysqli->close();
    }
?>
      <header id="main-header">
        <div class="darken">
            <div class="container">
                <h1>Registrace do nákupního seznamu</h1>
                <p>Hej babi, jak se jmenuje váš kocour?</p>
                <form method="post" action="#">
                    <div class="form-row">
                        <input type="text" name="forename" placeholder="Křestní jméno" required>
                        <input type="text" name="surname" placeholder="Příjmení" required>
                    </div>
                    <div class="form-row">
                        <input type="text" name="username" placeholder="Uživatelské jméno" required>
                        <input type="password" name="password" placeholder="Moje tajné heslo" required>
                        <input type="email" name="email" placeholder="Můj seuprímejl" required>
                    </div>
                    <input class='submit' type="submit" name="submit" value="Přihlásit">
                    <br/>
                    <a href='/' style='color: white; text-align: center;'>Už mám účet a chci se přihlásit!</a>
                </form>
                <?php
                    if($error != ''){
                        echo "
                    <div class='msg msg-error z-depth-3 scale-transition'>
                        $error
                    </div>";
                    }
                ?>
            </div>
        </div>
    </header>
    <div id="main">
        <div class="container">
            <?php
            if ($error != ''){
                ?><img src="https://www.copytrans.net/app/uploads/sites/3/2018/05/incorrect-password-meme-780x439.jpg"><?php
            }else{
                ?><img src="https://memeshappen.com/media/created/2018/07/YES-We-can-now-register.jpg"><?php
            }
            ?>
        </div>
    </div>
</body>
</html>
